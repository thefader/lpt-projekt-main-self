

// SCORING 
VAR samtalsfardigheter = 25
VAR overall_score = 0
VAR time_score = 30
VAR professionalism_score = 0
VAR kunskap_om_lpt = 50



=== fb_oscar
➡️ Här kommer lite återkoppling om dina beslut..


// ATTITYD

{agiterad_action_feedback:
    ~samtalsfardigheter = samtalsfardigheter-30
    ~time_score = time_score-21
    }



// BESLUT 

VAR stor_kunskapsbrist = false 
VAR sbar_forbattring_behovs = false 

//BAKJOUR 
{att_behalla_laglig:
~samtalsfardigheter = samtalsfardigheter - 10
}


// SCORE duzenlemesi
{professionalism_score< 1:
~professionalism_score = 1
}

{samtalsfardigheter< 1:
~samtalsfardigheter = 1
}

{time_score< 1:
~time_score = 1
}

{professionalism_score> 99:
~professionalism_score = 100
}

{samtalsfardigheter> 99:
~samtalsfardigheter = 100
}

{time_score> 99:
~time_score = 100
}

~overall_score = (professionalism_score+samtalsfardigheter+time_score)/3


-> final_rapport


VAR professionalism_betyg = ""
VAR samtalsfardigheter_betyg = ""
VAR tidseffektivitet_betyg = ""
VAR overall_betyg = ""

== final_rapport

{
- professionalism_score<=35 && professionalism_score>=30:
~professionalism_betyg = "A+" 

- professionalism_score<30 && professionalism_score>=25:
~professionalism_betyg = "A-" 

- professionalism_score<25 && professionalism_score>=21:
~professionalism_betyg = "B+" 

- professionalism_score<21 && professionalism_score>=16:
~professionalism_betyg = "B-" 

- professionalism_score<16 && professionalism_score>=12:
~professionalism_betyg = "C+" 

- professionalism_score<12 && professionalism_score>=8:
~professionalism_betyg = "C-"

- professionalism_score<8 && professionalism_score>=4:
~professionalism_betyg = "D+"

- else:
~professionalism_betyg = "D-"

}

{
- samtalsfardigheter<=60 && samtalsfardigheter>=51:
~samtalsfardigheter_betyg = "A+"

- samtalsfardigheter<51 && samtalsfardigheter>=43:
~samtalsfardigheter_betyg = "A-"

- samtalsfardigheter<43 && samtalsfardigheter>=35:
~samtalsfardigheter_betyg = "B+"

- samtalsfardigheter<35 && samtalsfardigheter>=27:
~samtalsfardigheter_betyg = "B-"

- samtalsfardigheter<27 && samtalsfardigheter>=19:
~samtalsfardigheter_betyg = "C+"

- samtalsfardigheter<19 && samtalsfardigheter>=11:
~samtalsfardigheter_betyg = "C-"

- samtalsfardigheter<11 && samtalsfardigheter>=3:
~samtalsfardigheter_betyg = "D+"

- else:
~samtalsfardigheter_betyg = "D-"
}


{
- time_score<=60 && time_score>=47:
~tidseffektivitet_betyg = "A+"

- time_score<46 && time_score>=38:
~tidseffektivitet_betyg = "A-"

- time_score<37 && time_score>=29:
~tidseffektivitet_betyg = "B+"

- time_score<29 && time_score>=21:
~tidseffektivitet_betyg = "B-"

- time_score<21 && time_score>=13:
~tidseffektivitet_betyg = "C+"

- time_score<13 && time_score>=5:
~tidseffektivitet_betyg = "C-"

- time_score<5 && time_score>=2:
~tidseffektivitet_betyg = "D+"

- else:
~tidseffektivitet_betyg = "D-"
}

VAR generiska = 0 

~generiska = (samtalsfardigheter+time_score)/2


VAR generiska_betyg = ""

{
- generiska<=55 && generiska>=51 && patAnamnes != low:
~generiska_betyg = "A+"

- generiska<=51 && generiska>=44 && patAnamnes != low:
~generiska_betyg = "A-"

- generiska<=44 && generiska>=38 && patAnamnes != low:
~generiska_betyg = "B+"

- generiska<=38 && generiska>=31 && patAnamnes != low:
~generiska_betyg = "B-"

- generiska<=31 && generiska>=23 && patAnamnes != low:
~generiska_betyg = "C+"

- generiska<=23 && generiska>=15 && patAnamnes != low:
~generiska_betyg = "C-"

- generiska<=15 && generiska>=8 && patAnamnes != low:
~generiska_betyg = "D+"

- else:
~generiska_betyg = "D-"
}




~overall_score = (time_score + professionalism_score+ samtalsfardigheter) / 3*2



{
- overall_score<=100 && overall_score>=90:
~overall_betyg = "A+"

- overall_score<90 && overall_score>=80:
~overall_betyg = "A-"

- overall_score<80 && overall_score>=70:
~overall_betyg = "B+"

- overall_score<70 && overall_score>=60:
~overall_betyg = "B-"

- overall_score<60 && overall_score>=50:
~overall_betyg = "C+"

- overall_score<50 && overall_score>=40:
~overall_betyg = "C-"

- overall_score<40 && overall_score>=30:
~overall_betyg = "D+"

- else:
~overall_betyg = "D-"
}


VAR inhamtadProcent = 45
~inhamtadProcent = (inhamtatInformation*100/67)


//BU KISIM CIKARILACAK
/*
Observationer: {observation_count}
Speglingar:{speglingAntal}
patAnamnes = {patAnamnes}
inhämtad info = {inhamtatInformation}
samtalsfärdigheter: = {samtalsfardigheter}
tidseffektivitet: = {time_score}
generiska: = {generiska}
overall_score: {overall_score}
*/
//BU KISIM CIKARILACAK


<center><h3>Overall betyg:  <b>{overall_betyg}</b> 
<center><i>Professionalism:</i>  <br><b>{professionalism_betyg}</b>
<center> <i>Övriga generiska färdigheter (tidseffektivitet, samtalsfärdigheter)</i><br> <b>{generiska_betyg}</b>
<center> <i>Inhämtad anamnes</i> <br> <b>{inhamtadProcent} %</b>
-> puan_yorum


== specifik_feedback
<b>Specifik feedback:</b>

// PATIENTANAMNES 
{patAnamnes == low:
    - Nästa gång försök att fokusera på att bli bättre på anamnestagning. Det finns olika sätt att göra det! <u>IG</u> sorry.
     {observation_count<4:
        - EXEMPEL Man kan ägna lite mer tid åt att observera patienten!
    }
    }

{patAnamnes == nope:
    - Nästa gång försök att fokusera på att bli bättre på anamnestagning. Det finns olika sätt att göra det!  
    {observation_count<4:
        - Man kan ägna tid åt att observera mer, exempelvis!
    }
    
}

{patAnamnes == average:
    - Anamnestagning var OK. Men det finns mer information som man kan hämta. Prova igen för en bättre score!
    {observation_count<4:
        - Man kan ägna tid åt att observera mer, exempelvis!
    }
    
}

{patAnamnes == high:
    - Anamnestagning var bra! Det finns endast lite förbättringsmöjlighet för detta. Bra jobbat! 
}

{patAnamnes == perfekt:
    - Anamnestagning var perfekt! Bra jobbat! 
}


// OBSERVERAR ELLER INTE 
{observation_count >= 3:
    - Det var mycket bra att du prioriterade att "observera" patienten!
}


// SBAR 
{sbar_forbattring_behovs:
    - Fokusera lite mer på förbättring inom SBAR.
}


// KUNSKAP_OM_LPT 
{stor_kunskapsbrist:
    - Det finns tecken på att du saknar mycket viktig information kring LPT-lagstiftning!
}



// HÄLSNING
{
    - halsar_utan_efternamn || halsar_utan_context || halsar_utan_fornamn:
    Tänk på att du bör hälsa professionellt på patienten, presentera dig med för- och efternamn och berätta vilken roll du har. Kommer du ihåg hur du gjorde?
}


{halsar_komplett:
    - Ditt sätt att hälsa på patienten var definitivt professionellt och respektfullt. Det är extra viktigt i en sådan här situation där patienten är rädd och känner sig utsatt. 
}

{fragar_roster:
    - Det är viktigt med att ha information om rösthallucinationer men tänk på att fråga detta utifrån ett sammanhang. Att fråga "Hör du röster?" utan någon kontext kan vara svårt att förstå för patienten och kanske lite provocerande!
}

{fragar_suicid_olampligt:
    - Du behöver öva dina färdigheter kring suicidriskbedömning! Tänk på suicidstegen och "timing" för frågor om suicid! Mycket viktigt att göra denna bedömning, men inte som första fråga! Att fråga om detta tidigt i samtalet är inte alliansskapande och kan även vara provocerande! 
}

{kontaktar_anhoriga:
    - Du behöver uppdatera dig lite kring "sekretess" inom vården. Patienten behöver ge sitt samtycke om vi ska kontakta anhöriga.
}


{fragar_om_brott:
    - Att fråga patienten om ett eventuellt brott tidigt i samtalet är ofta provocerande. 
}

{h1_konfronterar_direkt || fokuserar_pa_evidens:
    - Patientens uppfattningar bör inte ifrågasättas eller motbevisas tidigt i samtalet. Detta kan till och med vara provocerande och det är särskilt viktigt att undvika när man träffar en patient med psykos. Det betyder inte att man håller med patienten. Istället förhåller man sig neutralt och utforskande. 
}


{speglingAntal>=1:
    - Du har använt minst en <i>spegling</i> under samtalet! Vad bra! 
}



// FEEDBACK OM KUNSKAP OM LPT

{kunskap_om_lpt == 0:
    - Vissa av dina beslut kan betyda att du saknar mycket viktig kunskap om LPT-lagstiftningen! Spela mer! 
}


{not game_over_oldu:
    -> LPT_jour_kommer
}

{game_over_oldu:
    -> END
}



// Karin
{karin_oscar_far_inte_fatta_kvarh:
    - Vem får fatta kvarhållningsbeslut? Ta reda på detta!
}

{valjer_depot_inj:
    - Det finns vissa tvångsåtgärder man får göra efter att man fattar kvarhållningsbeslut om det behövs. Det får dock aldrig bli en tvångsmedicinering med ett läkemedel som är långverkande eller i depot form. 
}




// Bakjour

{bakjour_kommer_omedelbart_utan_tvang:
    - Som bakjour är det egentligen bättre med att avvakta med intagningsbeslut inom tidsramen om det är möjligt. Om man behöver använda tvångsåtgärder före intagningsbeslut, då måste man "omgående" ta ställning till intagning vilket självklart sker med en personlig bedömning av patienten.
}


{fattar_intagningsbeslut_per_telefon:
- Intagningsbeslut får aldrig fattas per telefon! 
}


{bakjour_kommer_inte_idag || bakjour_kommer_om_nagra_timmar:
- Om man behöver använda tvångsåtgärder före intagningsbeslut, då måste bakjour "skyndsamt" ta ställning till intagning vilket självklart sker med en personlig bedömning av patienten. Det går alltså inte att "avvakta" med denna bedömning. 
}

//
{bakjour_inte_intagn:
- Vården ska ske enligt HSL i alla möjliga mån. Men tror du att denna patient i detta skick kan få den vården som behövs? 
}


== puan_yorum
<center>ARBETET PÅGÅR.

* [Vill du höra lite specifik feedback?] -> specifik_feedback

{
- overall_score <100 && overall_score>=90:
<center><hr><h1>🙃</h1>Nästan perfekt! Det finns endast ett litet utrymme för förbättring.  
->specifik_feedback

- overall_score <90 && overall_score>=60: 
<center><hr><h1>🤔</h1>Verkar vara godkänd! Det finns dock utrymme för förbättring.  
->specifik_feedback

- overall_score <60 && overall_score>=40: 
<center><hr><h1>😕</h1>Det finns utrymme för förbättring. Försök förbättra din prestation! 
->specifik_feedback

- overall_score <40: 
<center><hr><h1>😯</h1> Det finns ganska mycket utrymme för förbättring. Kör gärna i flera omgångar! <br><br>Vi vet att du kan prestera bättre!
<hr>
->specifik_feedback

- else:
<hr><h1>🤩🏆🥇</h1>Perfekt! Det verkar att du kan det här!
->specifik_feedback
}

