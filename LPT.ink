// LPT Gamifierat undervisningsspel av Direnc Sakarya
// 2018. UU. 

INCLUDE h1.ink
INCLUDE h2.ink
INCLUDE h3.ink
INCLUDE 1_vi.ink
INCLUDE feedback_oscar.ink
INCLUDE h1_drWinblad
INCLUDE h1_bakjour



LIST patientState = lugn, motvillig, upprord, agiterad
LIST agitationState = (off), on 
LIST doktorState = snabb, detaljinriktad, friendly 
VAR inhamtatInformation = 0
LIST patAnamnes = (low), nope, average, high, perfekt






//svarar eller inte
VAR antalet_ej_svarade = 0


// Variabler för hälsningsval
VAR halsar_utan_efternamn = false
VAR halsar_komplett = false
VAR halsar_utan_fornamn = false
VAR halsar_utan_context = false


// Variabler för samtal efter hälsning
VAR kollar_om_pat_behover_ngt = false
VAR fragar_varfor_pat_ar_har = false 
VAR fragar_hur_man_kan_hjalpa_pat = false
VAR fragar_direkt_till_anhoriga = false
VAR vill_pat_berattar_allt = false
VAR fragar_roster = false
VAR fragar_om_pat_vill_ata_nagot = false 


// Variabler för perseveration
VAR fragar_vad_menar_du = false
VAR fragar_om_brott = false

// Variabler för grannberättelse
VAR jag_tror_pa_dig_johan = false
VAR fragar_mordades = false

// Variabler för lite mer om grannberättelse
VAR fokuserar_pa_utomjord = false
VAR fokuserar_pa_evidens = false
VAR fokuserar_pa_apparat = false

// Variabler för hantering av kvarhållning
VAR oppnar_dorren = false
VAR far_inte_ga = false
VAR skriver_vardintyg = false
VAR kontaktar_bakjour_fore_vardintyg = false
VAR kontaktar_ST_lakare = false


VAR h1_kontaktar_bakjour_utan_information = false 


// Variabler för samtal med bakjour före vårdintyg
VAR jag_kan_inte_bedoma = false
VAR komplex_SBAR_kallar_bakjour = false
VAR god_SBAR = false 




// Variabler för VI
// 1_vi_1
VAR valjer_paragraf_4 = false
VAR valjer_paragraf_11 = false
VAR valjer_paragraf_5 = false

// 1_vi_2
VAR valjer_id_kort = false
VAR valjer_att_inte_skriva_nagot = false
VAR valjer_att_undersoka_vidare = false

// 1_vi_3
VAR bakgrund_skriver_aktuellt = false
VAR bakgrund_skriver_behov_att_stanna = false
VAR bakgrund_skriver_korrekt = false
VAR bakgrund_skriver_kommer_med_polis = false


// 1_vi_4
VAR aktuellt_bedomer = false
VAR aktuellt_korrekt = false 
VAR aktuellt_kort_daligt = false
VAR aktuellt_bedomer_men_ok = false 


VAR hasta1 = false
VAR hasta2 = false
VAR hasta3 = false

-> entrance



// BESKRIVNING AV SCENEN
== entrance
//Beskrivning av situation, egen titel, uppdraget. 
# images/sjukhus.png

Ett helt vanligt jourpass på psyk... 
# fa:fas fa-user-md fa-2x
Oscar närmar sig slutet av sin allmäntjänstgöring. 

Han jobbar ikväll som ensam läkare på psykakuten. Han har blivit informerad att det finns en "ST-läkare i psykiatri" och en "psykiatri bakjour" som är tillgängliga om han behöver hjälp. <br>

Passet har hittills varit ovanligt lugnt. 
...

-> sacmalik_randomiserare

== sacmalik_randomiserare
{~Oscar bestämmer sig för att ta en kopp kaffe. | Nu finns det inte någon patient. Oscar tänker titta på senaste riktlinjerna om behandling av depression. | Oscar tänker på vad han ska göra efter jourpasset imorgon. | Oscar börjar dagdrömma om hur livet ska bli efter att han får sin läkarlegitimation. | Oscar börjar dagdrömma om det dataspel som Ali tipsade honom om. | Oscar bestämmer sig att passa på att signera sina gamla anteckningar. | Oscar bestämmer sig för att gå till fikarummet och småprata med personalen som är där. | Oscar bestämmer sig för att ringa sin pojkvän. | Oscar börjar läsa aktuell information på Navet. | Oscar bestämmer sig för att kolla sin e-mail. } 
-> tam_da_onu_yapacakken

== tam_da_onu_yapacakken
<b>Men...
-> telefonen_ringer


// TELEFONEN 
== telefonen_ringer
<center>
# fa:fas fa-mobile-alt fa-spin fa-2x
#color: red
{!Nu är det någon som ringer.| ...Telefonen fortsätter att ringa!| Det verkar inte vara ett klokt sätt att hantera detta jourpass.   } </center>
* [Oscar svarar. ] 
//# fa:fas fa-ellipsis-h fa-2x 
-> randomization
+ [Oscar avvaktar med att svara.] -> vantar_pa_att_svara 

// RANDOMIZATION
===== randomization =====
-> hasta1_secildi
//{~ ->hasta1_secildi | ->hasta2_secildi | ->hasta3_secildi} 
//-> DONE



== hasta1_secildi
~hasta1 = true
-> hasta1_inledning
-> DONE

== hasta2_secildi
~hasta2 = true
-> hasta2_inledning

== hasta3_secildi
~hasta3 = true
->hasta3_inledning



== vantar_pa_att_svara
{antalet_ej_svarade >= 2: -> finito}
{Oscar är lite osäker om det är en bra idé, men väljer ändå att avvakta med att svara.|Han vet inte varför han gör det, men han avvaktar.}
# images/waiting.gif
~antalet_ej_svarade = antalet_ej_svarade + 1
~time_score = time_score-10
//{cevaplamama_sayisi}
~professionalism_score=professionalism_score-33
->telefonen_ringer

== finito
    Det är nog bäst att Oscar meddelar att han inte kan arbeta idag. 
    <br>
# images/gameover.png
<br>
<center><i>Vad kul att du provade spelet! <br>

<center>Lycka till med ditt nästa försök! 

<center>Här kommer din "bonus"😎 :<br> Professionalism score: <b>-184</b>/100 poäng! <br></center>
->END




