(function(storyContent) {

    var story = new inkjs.Story(storyContent);

    var storyContainer = document.querySelectorAll('#story')[0];

    function showAfter(delay, el) {
        setTimeout(function() { el.classList.add("show") }, delay);
    }

    function scrollToBottom() {
        var progress = 0.0;
        var start = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
        var dist = document.body.scrollHeight - window.innerHeight - start;
        if( dist < 0 ) return;

        var duration = 300 + 300*dist/100;
        var startTime = null;
        function step(time) {
            if( startTime == null ) startTime = time;
            var t = (time-startTime) / duration;
            var lerp = 3*t*t - 2*t*t*t;
            window.scrollTo(0, start + lerp*dist);
            if( t < 1 ) requestAnimationFrame(step);
        }
        requestAnimationFrame(step);
    }

    function continueStory() {

        var paragraphIndex = 0;
        var delay = 0.0;

        // Generate story text - loop through available content
        while(story.canContinue) {

     	   // Get ink to generate the next paragraph
		    var paragraphText = story.Continue();

             // Create paragraph element
            var paragraphElement = document.createElement('p');
            paragraphElement.innerHTML = paragraphText;
            storyContainer.appendChild(paragraphElement);


            var tags = story.currentTags;
            for(var i=0; i<tags.length; i++) {
                var tag = tags[i];
                if( tag.endsWith(".png") || tag.endsWith(".jpg") || tag.endsWith(".gif") ) {
                    var img = document.createElement('img');
                    img.src = tag;
					img.height = "256";
					img.width = "256";
					img.classList.add("center");
					//document.getElementById("resim").appendChild(img);

 					storyContainer.appendChild(img);

                }

				//Font-Awesome
				if (tag.startsWith("fa:")) {
					var icon = document.createElement("i");
					iconname=tag.split(":")[1];
					icon.className=iconname;
					paragraphElement.appendChild(icon);
				}


				//Change color of text
				if (tag.startsWith("color:")) {
					textcolor=tag.split(":")[1];
					paragraphElement.style.color = textcolor;
				}
            }


			// Fade in paragraph after a short delay
            showAfter(delay, paragraphElement);

            delay += 200.0;
        }

        // Create HTML choices from ink choices
        story.currentChoices.forEach(function(choice) {

            // Create paragraph with anchor element
            var choiceParagraphElement = document.createElement('p');
            choiceParagraphElement.classList.add("choice");
            choiceParagraphElement.innerHTML = `<a href='#'>${choice.text}</a>`
            storyContainer.appendChild(choiceParagraphElement);

            // Fade choice in after a short delay
            showAfter(delay, choiceParagraphElement);
            delay += 200.0;

            // Click on choice
            var choiceAnchorEl = choiceParagraphElement.querySelectorAll("a")[0];
            choiceAnchorEl.addEventListener("click", function(event) {

                // Don't follow <a> link
                event.preventDefault();

                // Remove all existing choices
                var existingChoices = storyContainer.querySelectorAll('p.choice');
                for(var i=0; i<existingChoices.length; i++) {
                    var c = existingChoices[i];
                    c.parentNode.removeChild(c);
					//document.getElementById("resim").style.visibility = "hidden";


					// REMOVE IMAGE
					var imglist = document.getElementById("resim");
					if (imglist.hasChildNodes()) {
					    imglist.removeChild(imglist.childNodes[0]);
					}


					// Fade previous text and image
					paragraphElement.style.opacity = 0.2;
					if (img){
						img.style= "opacity: 0.2";
					}




				}

                // Tell the story where to go next
                story.ChooseChoiceIndex(choice.index);

                // Aaand loop
                continueStory();
            });
        });

        scrollToBottom();
    }

    continueStory();

})(storyContent);
