
== hasta2_inledning

Jag ser en man, pratar med sina närstående. 

Jag närmar mig för att hälsa honom välkommen. 

* [Hej, mitt namn är Ebba. Välkommen.]
<i>"Hej Ebba! Kul att träffa dig!" </i>säger patienten med ett stort leende. Vänder sig mot sina anhöriga och frågar:
<i>"Visst hette hon Ebba också som hjälpte oss med trädgården?" </i> Jag noterar en mycket trött blick hos patientens närstående. 
~halsar_utan_efternamn = true
~halsar_utan_context = true

-> forsta_samtalet

* [Hej, Ebba Dalqvist. Läkare.]
<i>"Hej läkare! Vad roligt! Ska vi undersöka någon? Jag kan egentligen göra det!" </i> säger patienten ganska fort. <br>
Jag noterar en mycket trött blick hos patientens närstående. <br>

-> forsta_samtalet

* [Hej, Jag heter Ebba Dalqvist. Jag jobbar som läkare här.]
~halsar_komplett = true
<i>"Hej läkare Ebba! Vad roligt! Jag heter Magnus. Ska vi gå ut tillsammans? </i>🤣<i> Nejj, jag skojar bara!</i>" säger patienten ganska fort. 
<br>

-> forsta_samtalet

* [Hej.]
~halsar_utan_efternamn = true
~halsar_utan_context = true
~halsar_utan_fornamn = true
<i>"Hej på dig den mystiska personen!</i>" säger patienten ganska fort. 

-> forsta_samtalet



=== forsta_samtalet
* [Vill du berätta för mig varför du är här idag?]
"<i>Visst! Det är den här <b>fucking idioten</b> (skriker) som körde oss hit, det är mitt problem. Sådana typer får inte vara här egentligen, jag är absolut inte racist, men du vet hur det är med sådana. Jag blir inte lurad så enkelt. Idag är inte min födelsedag, men ändå. Tänk om hur roligt det hade varit om det var min födelsedag idag! (<b>skrattar högt</b>).</i>" <br><br> Patientens fru avbryter honom: "Får jag berätta?" 


-> ska_narstaende_beratta

* [Kan du berätta lite om idag?] 
"<i>Idag? idag? Det var en sådan vanlig dag idag. Med solen och allt. <b> Men faaaaaan! </b> Jag vill verkligen inte slösa tid med det här. Ursäkta kära läkare, du är söt. Men jag har mycket annat att göra."</i> <br><br> Patientens fru avbryter honom: "Får jag berätta?" 

-> ska_narstaende_beratta

* [Kan någon av er (närstående) berätta för mig vad han har för besvär?]
"<i><b>Va? va? Vad snackar du om? Jag är ju här!"</b> </i><br><br> ... Jag noterar att han har blivit mycket arg efter denna fråga. <br><br>Patientens fru flikar in: "Får jag berätta?" 
~fragar_direkt_till_anhoriga = true


-> ska_narstaende_beratta

* [Hur kan jag hjälpa dig?]
~fragar_hur_man_kan_hjalpa_pat = true
"<i>Frågan är egentligen, hur kan <b>jag</b> hjälpa dig? Är du beredd att få min hjälp? Tror inte." 
Patientens fru avbryter honom: "Får jag berätta?" 
-> ska_narstaende_beratta




* [Jag vill att du känner dig trygg. Du kan berätta allt för oss här. Varsågod.]
"<i>Nämen vad trevligt! Ska jag nu gå tillbaka till barndomen? (skrattar högt) Var vill du att jag berättar först? Ska jag börja med hur farsan och morsan knullade varandra?"<br><br></i> Patientens fru avbryter honom: "Får jag berätta?" 
~vill_pat_berattar_allt = true 

-> ska_narstaende_beratta


=== ska_narstaende_beratta
* [Jag vill helst prata lite mer med honom. Vill du fortsätta?]
"<i>Nämen självklart! Du vill kanske att de andra lämnar rummet först? Whatever! Var var vi? Jo, jag kan förstå att du blev imponerad av min charm. I can sing all the love songs for you! </i> 🎤🎤🎤 ... (sjunger)..."
-> ber_om_tillatelse_for_anhorigsamtal


=== ber_om_tillatelse_for_anhorigsamtal
* [Jag vill prata också med dina anhöriga, är det OK?]
🎤🎤🎤 ... (sjunger "OK" som en låt)
Jag tar det som en ja.

-> patientens_fru_berattar


=== patientens_fru_berattar
Jag vänder mig mot patientens fru som väntar på att berätta. Hon börjar: <br><br>
"Tack! Jag ber om ursäkt. Han har varit såhär sedan några dagar tillbaka. Vi vet att han har manodepressiv sjukdom, men han har aldrig blivit såhär illa innan."
<br>
Patienten avbryter sin fru och frågar:<br>
<i>"Nu är det dags att åka härifrån. Tack för ett trevligt och ett kort möte. Det är väl bäst när det är kort."
-> vill_ga_darifran_2


=== vill_ga_darifran_2
Patienten är mycket bestämt. Han vill gå. Går mot porten. Anhöriga tittar på mig, jag kan notera att de är extremt oroliga. Vad ska vi göra nu? 

* [Patienten har rätt att gå. Man får inte tvinga honom att stanna.]
-> game_over

* [Jag ska försöka övertala honom att stanna.]
-> overtala_manisk_patient_att_stanna

* [Patienten stannar.]
-> patienten_stannar






== overtala_manisk_patient_att_stanna
* [Magnus, jag tycker att det är bäst för dig att vara kvar här ett tag. Vi behöver observera dig. Jag tänker prata med mina mer erfarna kollegor. Är det OK?]
... säger jag med min lugnaste röst. 
<br>
"<i>OK. Let's go my dear!"</i> säger han. Patienten blir inlagd på den akuta avdelningen. Jag dikterar en inskrivningsanteckning enligt HSL, och tar ett fika. 😎 Skål!


-> en_halv_timme_senare

=== en_halv_timme_senare
<br><br><br><br><br><br><br><br><br>
<center>En halv timme senare...
<br><br><br>
-> blir_inlagd_HSL_men_vill_ga

== blir_inlagd_HSL_men_vill_ga
Sjuksköterskan på avdelningen ringer och berättar att patienten har sina väska framme och vill skriva ut sig. Står och väntar på mig vid porten. Han är arg. Skriker. Vill skriva ut sig genast. Vad gör vi nu? 

-> vad_gor_vi_nu


==== vad_gor_vi_nu
// ICERIK DOLACAK
-> DONE






== patienten_stannar
Jag bedömer att patienten måste stanna. Han får alltså inte lämna sjukhuset trots att han vill det. Men jag behöver lagstödet bakom det här beslutet. Hur bör jag gå vidare?

* [Jag måste skriva vårdintyg.]
-> skriv_vardintyg

* [Jag måste kalla in någon som får skriva vårdintyg.]
-> kalla_in_ngn





== skriv_vardintyg
// POÄNGJUSTERING
Jag känner mig inte super-erfaren med detta. Är egentligen lite osäker om jag <i>får</i> skriva vårdintyg. Hur bör jag göra?

* [Jag skriver vårdintyg. Patienten måste stanna.]
-> game_over

* [Kolla med bakjouren om detta.]
-> ring_bakjour_fore_vardintyg2





== ring_bakjour_fore_vardintyg2
"Hej, det är Ebba här, primärjour."
"Hej, Ebba. Hur kan jag hjälpa dig?"

* [Det är ju såhär. SBAR1]
"RESPONS 1"
"Tack för hjälpen."

Nu är det dags att kalla in en leggad kollega. -> kalla_in_ngn

* [Det är ju såhär. SBAR2]
"RESPONS 2"
"Tack för hjälpen."

Nu är det dags att kalla in en leggad kollega. -> kalla_in_ngn


* [Det är ju såhär. SBAR3]
"RESPONS 3"
"Tack för hjälpen."

Nu är det dags att kalla in en leggad kollega. -> kalla_in_ngn

-> DONE



== kalla_in_ngn
<br><br><br><br>Jag ringer en kollega som har läkarlegitimation. 
Hon heter Efsun.
-> efsun_skriver_vardintyg


== efsun_skriver_vardintyg
-> DONE




-> DONE

-> END
