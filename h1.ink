

VAR laser_journal_oversiktligt = false 
VAR laser_journal_utforligt = false 
VAR gar_in_direkt_utan_journal = false 


// BESKRIVNING
== hasta1_inledning 
# images/nurse.png
Det är Bertil, sjuksköterska på expeditionen. <br><br>Han ringer om en patient som heter <b>Johan Grönqvist</b>. Han har inkommit med polis. Bertil önskar att Oscar kommer dit. 
-> hast1_inl_sonr_val



== hast1_inl_sonr_val
Oscar väljer nu att ... # fa:fas fa-arrow-circle-down

* {not laser_journal_utforligt} [... läsa patientens journal <i>översikligt</i> innan han går in.]
# fa:fas fa-notes-medical fa-2x
<b> Oscar öppnar patientens journal. </b>
# images/records.png
Patienten har en aktuell schizofrenidiagnos.... OK.... Han blev ordinerad <i>Risperidon 4 mg</i> men uteblev från de två senaste återbesöken.... Hmmm..... Patientens bror har schizofreni och behandlas med Klozapin..... Inget drogmissbruk finns uppgivet. 
~inhamtatInformation = inhamtatInformation + 7
~time_score += 5
~laser_journal_oversiktligt = true 
~laser_journal = true 
-> hast1_inl_sonr_val



* [... att läsa patientens journal <i>utförligt</i> innan han går in.]
# fa:fas fa-notes-medical fa-2x
<b> Oscar öppnar patientens journal och börjar med anteckningar från barndomen.</b>
# images/records.png
~inhamtatInformation = inhamtatInformation + 10
~time_score += -5
~laser_journal_utforligt = true 
~laser_journal = true 
Första kontakten med psykiatri är i ung vuxen ålder... I journalen finns information om att han uppfattades som lite "udda" av sina vänner i barndomen. Får se... Vissa anteckningar finns inte här, utan de är scannade. Oscar vill titta på de anteckningarna också och går in i KOVIS... Oj, här finns ganska många anteckningar som tillhör BUP. Där finns det information om att man gjorde en neuropsykiatrisk utredning på grund av autism-misstanke men utredningen ledde inte till någon diagnos.
...

...

(ca 20 minuter senare...)<br>

<b>Oscar får ett nytt telefonsamtal från Bertil, han undrar om han kommer eller inte! Oscar säger att han kommer strax! </b>

Patienten har en schizofrenidiagnos och en aktuell ordination av <i>Risperidon 4 mg</i> dagligen. Patientens bror har schizofreni och behandlas med Klozapin..... Inget drogmissbruk finns uppgivet. 
-> hast1_inl_sonr_val


* [... gå in i rummet.] 
<b> Oscar går in i rummet. </b># fa:fas fa-door-open fa-2x 
# images/patient.png
~gar_in_direkt_utan_journal = true 
~time_score += 10 
Han ser en patient som står vid britsen. Två poliser står bredvid honom. Han.. alltså patienten verkar "ointresserad"... <br>och sliten...  
Utan tvekan ter sig polismännen jäktade.
-> 1_entre





== efter_journal_gar_in
Oscar går in i rummet.
# images/patient.png
Han ser en patient som står vid britsen. Två polismän står bredvid honom. Han.. alltså patienten verkar "ointresserad"... <br>och sliten...  
Utan tvekan ter sig polismännen jäktade.
-> 1_entre


VAR observerar = false
VAR otillracklig_anamnes = false 
VAR patientkortet_kollar = false 
VAR kontaktar_anhoriga = false 
VAR rapport_fran_polisen_fatt = false 



=== 1_entre ===

Oscar väljer nu att ... # fa:fas fa-arrow-circle-down

    * [... ta emot polisens rapport]
    ~rapport_fran_polisen_fatt = true 
    -> h1_rap_fr_polisen

    * [... observera patienten närmare.]
    -> h1_observera

    * [... kolla patientkortet för att få mer information.]
    ~patientkortet_kollar = true 
    Oscar tittar på patientkortet och ser att patienten är bokförd i Uppsala och att det finns kontaktinformation till patientens båda föräldrar. <br>
    -> 1_entre

    * [... prata med patienten] 
    <b>Oscar väljer att prata med patienten och närmar sig patienten. </b>
    Nu står de mitt emot varandra, patienten vänder sig  lite mot Oscar, men tittar honom inte direkt i ögonen. <br><br>
    Oscar ska hälsa på honom. 
    -> hasta1_samtal_halsning

    * {rapport_fran_polisen_fatt} [... kontakta bakjour.]
    <b>Oscar väljer att kontakta bakjour?? Nej.<br></b>
    Njaaa... Han ska nog inte göra det utan att veta mer om patienten. 
    ~h1_kontaktar_bakjour_utan_information = true 
    ~kunskap_om_lpt = kunskap_om_lpt-10
    -> 1_entre
    
    * [... göra en suicidriskbedömning]
    <b>Oscar väljer att göra en suicidriskbedömning, men... </b>
    Nej. Han vet nästan ingenting om patienten. Vill inte börja samtalet med att fråga om det.
    ~fragar_suicid_olampligt = true 
    -> 1_entre

   * {patientkortet_kollar} [... kontakta anhöriga] 
   ~kontaktar_anhoriga = true 
   ~kunskap_om_lpt = kunskap_om_lpt-5
    Oscar väljer att kontakta anhöriga men Bertil påminner honom om att han inte får göra det utan patientens samtycke. Just det... 
    Oscar tänker först fråga patienten om tillåtelse, men det är kanske bäst att gå vidare med anamnestagning.  
    -> 1_entre





// RAPPORT FRÅN POLISEN 
== h1_rap_fr_polisen
# images/police.png

<b>Oscar väljer att ta emot polisens rapport. # fa:fas fa-comment fa-2x
</b>
Polisen berättar att patienten hittades i Stadsskogen under eftermiddagen. Han hade varit försvunnen sedan några dagar tillbaka och hade rapporterats av familjen som saknad. 

När polisen såg honom i skogen, försökte han smita, oklart varför. Polisen försökte prata med honom men han blev mycket rädd, mumlade och uppförde sig "märkligt". Polisen visste redan att han har varit en patient inom psykiatri, så de valde att ta med honom dit. 
~inhamtatInformation = inhamtatInformation+8
~time_score += 5

Oscar väljer nu att... # fa:fas fa-arrow-circle-down

* {not laser_journal_utforligt} [... fråga polisen om de vet något mer.] 
~professionalism_score = professionalism_score+5
<b>Oscar frågar om polisen vet något mer.</b>

Poliserna beskriver att familjen berättat att patienten haft kontakt med psykiatrin och de var oroliga över att han kanske skulle skada sig själv, eftersom han tidigare sagt något om detta. 

Poliserna säger att det är allt.  

De står ändå och väntar lite till. 

Oscar tackar polisen för rapporten.
~inhamtatInformation = inhamtatInformation+6
-> 1_entre
* [... tacka polisen för rapporten.] <b> Oscar tackar polisen för rapporten.</b>
-> 1_entre






// OBSERVERA 
VAR observation_onemsiyor = false
VAR observation_onemsemiyor = false 

== h1_observera
~observerar = true 
# images/observe.png
// HÄR KAN DET KOMMA EN BILD 
<b>Oscar väljer att observera patienten närmare.</b> 

-> observera_i_detalj

VAR observation_count = 0 
VAR observerar_inte_allt = false 

== observera_i_detalj
~professionalism_score = professionalism_score+2
~observation_count = observation_count + 1
~inhamtatInformation = inhamtatInformation + 2
{!<i>Han noterar att det luktar ganska ofräscht i hela rummet. |<i> Patienten har bara en tunn tröja på sig trots att det är mycket kallt ute. | <i>Patientens tröja har smuts och fläckar överallt. Kan det vara blod? Nja.. |<i> Patienten står och tittar mot väggen med tom blick. Det verkar som han mumlar, Oscar lyssnar på honom men det är mycket svårt att förstå. Går det han säger att begripa? ...| <i>Patienten ter sig helt ointresserad, nästan "apatisk".| <i>Oscar ser inte något annat av intresse.}
+ {observation_count<=5} [Oscar antecknar om denna observation och fortsätter att observera.]
-> observera_i_detalj
* [Oscar tycker att det är relevant, antecknar dessa observationer och väljer att gå vidare med anamnestagning.]
~observation_onemsiyor = true 
-> 1_entre 
* [Oscar tycker inte att fortsatta observationer är prioriterade utan väljer att gå vidare med anamnestagning.]
~observation_onemsemiyor = true 
-> 1_entre 






/// PRATA MED PATIENTEN  
VAR jag_ar_har_att_hjalpa = false 
VAR agitationsprovocerare = 0



// 1. HÄLSA 
== hasta1_samtal_halsning

# images/talk.png
* <b>"Hejsan Johan, jag heter Oscar, läkare. Välkommen." [] </b>säger Oscar.
~halsar_utan_efternamn = true
~professionalism_score = professionalism_score+1
~samtalsfardigheter = samtalsfardigheter-1
Ingen respons... # fa:fas fa-volume-mute
Som om att han inte ens hörde honom. 
~inhamtatInformation = inhamtatInformation + 1
Oscar väljer att fortsätta med bedömningen. -> hasta1_samtal_efter_halsning

* <b>"Hej, jag heter Oscar Johansson, läkare. Välkommen."</b> [] säger Oscar.
~professionalism_score = professionalism_score+2
~inhamtatInformation = inhamtatInformation + 2
~samtalsfardigheter = samtalsfardigheter+2
Tittar lite mot Oscar, men fortsätter att mumla. Mycket svårt att förstå vad han säger. 
Oscar väljer att fortsätta med bedömningen. -> hasta1_samtal_efter_halsning

* <b> "Tja, läget?"[] </b> säger Oscar.
~halsar_utan_context = true
~agitationsprovocerare = agitationsprovocerare + 2
~professionalism_score = professionalism_score-5
~samtalsfardigheter = samtalsfardigheter-5
Tittar mot Oscar med frågande blick. Han ser tydligen inte ut nöjd att vara där. 
Oscar väljer att fortsätta med bedömningen. -> hasta1_samtal_efter_halsning

* <b>"Hejsan, Oscar Johansson heter jag. AT-läkare. Välkommen."</b> [] säger Oscar.
~halsar_komplett = true 
~inhamtatInformation = inhamtatInformation + 2
~professionalism_score = professionalism_score+3
~samtalsfardigheter = samtalsfardigheter+4
Tittar lite mot Oscar, men fortsätter att mumla. Mycket svårt att förstå vad han säger. 
Oscar väljer att fortsätta med bedömningen. -> hasta1_samtal_efter_halsning

* <b>"Hej Johan, välkommen hit!"[] </b>säger Oscar.
~halsar_utan_fornamn = true
~agitationsprovocerare = agitationsprovocerare + 1
~inhamtatInformation = inhamtatInformation + 1
~samtalsfardigheter = samtalsfardigheter-1
Tittar mot Oscar med frågande blick. Han ser tydligen inte ut nöjd att vara där. 
Oscar väljer att fortsätta med bedömningen. -> hasta1_samtal_efter_halsning

* <b> "Hej, hur mår du?"[] </b> frågar Oscar.
~halsar_utan_fornamn = true
~halsar_utan_context = true
~professionalism_score = professionalism_score-2
~agitationsprovocerare = agitationsprovocerare + 1
~samtalsfardigheter = samtalsfardigheter-3
Tittar inte på Oscar. Oklart om han hörde vad Oscar sa. Mumlar något som inte går att förstå. -> hasta1_samtal_efter_halsning

* <b> "Hej, mitt namn är Oscar Johansson. Jag är läkare. Jag är här för att hjälpa dig."[] </b> säger Oscar.
~halsar_komplett = true
~jag_ar_har_att_hjalpa = true 
~inhamtatInformation = inhamtatInformation + 1
~samtalsfardigheter = samtalsfardigheter+1
Tittar inte på Oscar. Oklart om han hörde vad han sa. Mumlar något som inte går att förstå. -> hasta1_samtal_efter_halsning





// 2. SAMTALET FORTSÄTTER
VAR h1_hor_du_vad_jag_sager = false
VAR h1_onskar_hora_allt_fran_borjan = false 
VAR fragar_suicid_olampligt = false

== hasta1_samtal_efter_halsning

* Hör du vad jag säger? [] frågar Oscar.  
~h1_hor_du_vad_jag_sager = true
Ingen respons. Han fixerar blicken mot väggen och mumlar.
~inhamtatInformation = inhamtatInformation + 1
-> hasta1_samtal_efter_halsning

* "Är du trött? Vill du sitta lite?" [] frågar Oscar. Han ser ganska sliten ut. 
~kollar_om_pat_behover_ngt = true
~professionalism_score = professionalism_score+3
~samtalsfardigheter = samtalsfardigheter+3
Han tittar på Oscar och säger "Snälla, jag är oskyldig, låt mig vara i fred." 
~inhamtatInformation = inhamtatInformation + 2
-> perseveration

* "Kan du berätta för mig varför du är här idag?" [] frågar Oscar.
~fragar_varfor_pat_ar_har = true
~inhamtatInformation = inhamtatInformation + 1
~professionalism_score = professionalism_score+1
~samtalsfardigheter = samtalsfardigheter+1
Han fortsätter att mumla. Oscar tror inte att han hör något som han kan förstå i när patienten talar. 
<br>
Det verkar att Oscar inte kommer kunna ha ett samtal med honom. 

Han undrar om patienten kan förstå vad han säger om han fokuserar på det.
-> perseveration

* "Hör du röster?" [] frågar Oscar.
~agitationsprovocerare = agitationsprovocerare + 1
~fragar_roster = true
~samtalsfardigheter = samtalsfardigheter-3
Inget svar.<br>
Det verkar att Oscar inte kommer kunna ha ett samtal med honom. 
Han undrar om han kan förstå vad han säger om han fokuserar på det.
-> perseveration

* "Har du tankar på att ta livet av dig?" [] frågar Oscar.
~agitationsprovocerare = agitationsprovocerare + 1
~fragar_suicid_olampligt = true
~professionalism_score = professionalism_score-3
~samtalsfardigheter = samtalsfardigheter-5
Han tittar mot Oscar och säger: <br> "Du är en av dem. Tycker ni att ser ut som en idiot?"
-> vill_ga_darifran







VAR agiterad_action_feedback = false

== agiterad_action
# fa:fas fa-exclamation-circle fa-3x
~agiterad_action_feedback = true 
# images/agitation.png
<b>Patienten blir agiterad! </b><br><br>Skriker mycket högt! Poliserna håller patienten i armarna. 

Det går uppenbarligen inte att fortsätta med samtalet. 

Sjuksköterskan tittar på Oscar frågande. 

Vad ska han göra nu? 

-> vill_ga_darifran








== perseveration
{agitationsprovocerare >=3:
    - -> agiterad_action
}
-> h1_samtal_devam



VAR h1_onskar_hora_allt_fran_borjan_barndom = false 
VAR speglingAntal = 0
VAR direktUppmuntran = 0 




== h1_samtal_devam

{agitationsprovocerare >=3:
    - -> agiterad_action
}
    Oscar uppfattar frasen som patienten säger medan han mumlar oavbrutet.
Han säger <b>"jag är oskyldig"</b> om och om igen. Ordena har blivit lite förstörda eftersom han upprepar detta jämt. Oscar tittar på patienten med frågande blick. 

* <b>""Oskyldig?" Berätta mer..."</b> [] frågar Oscar.
# images/question.png
~fragar_vad_menar_du = true
~speglingAntal += 1
~direktUppmuntran += 1
~professionalism_score = professionalism_score+2
~inhamtatInformation = inhamtatInformation + 5
~samtalsfardigheter = samtalsfardigheter+5
Han talar mycket fort och säger <i>att det inte var han som dödade grannarna.<i> Det är svårt att förstå alla ord, men Oscar begriper i alla fall att han säger "dödade grannarna". 
~inhamtatInformation = inhamtatInformation+5
-> h1_samtal_devam_2

* <b>"Har du begått ett brott?"</b> [] frågar Oscar. 
# images/question.png
~fragar_om_brott = true
~agitationsprovocerare = agitationsprovocerare + 1
~professionalism_score = professionalism_score-2
~samtalsfardigheter = samtalsfardigheter-3
~time_score += -3
~kunskap_om_lpt = kunskap_om_lpt-3

Oscar noterar att patienten blir lite mer upprörd och frustrerad efter denna fråga. 
Han börjar skrika: <b>"Nej! Nej! Jag är oskyldig! Jag är oskyldig!" 
-> h1_samtal_devam_2


* <b>"Nu måste jag förstå det här ordentligt. Berätta allt från början."</b> [] säger Oscar. 
~h1_onskar_hora_allt_fran_borjan = true 
~samtalsfardigheter = samtalsfardigheter-2
Oscar noterar att polisen börjar bli lite rastlös, de har tydligen inte mycket tid. 

Patienten pratar mycket snabbt, det är fortfarande ganska svårt att förstå vad han säger. Han pratar med viss entusiasm och man kan se att han är mycket "inne" i berättelsen. Men svårt att förstå vad han säger. 

Oscar försöker använda alla sina samtalsfärdigheter under samtalet. Men nej, det går inte. Han pratar, men Oscar förstår inte. Det verkar inte finnas en tydlig röd tråd. Efter ca 10 minuter, skriker patienten plötsligt:

    "De är döda!"
    ~inhamtatInformation = inhamtatInformation+5
-> h1_samtal_devam_2

* <b>"Nu måste jag förstå det här ordentligt. Berätta allt från början. Börja med din barndom."</b> [] säger Oscar. 
~h1_onskar_hora_allt_fran_borjan_barndom = true 
~professionalism_score = professionalism_score-2
~samtalsfardigheter = samtalsfardigheter-5
~time_score += -5
~kunskap_om_lpt = kunskap_om_lpt-3 
Oscar noterar att polisen börjar blir rastlös, de har tydligen inte mycket tid. 

Patienten pratar mycket snabbt, det är fortfarande ganska svårt att förstå vad han säger. Han pratar med viss entusiasm och man kan se att han är mycket "inne" i berättelsen. Men svårt att förstå vad han säger. 

Oscar försöker använda alla sina samtalsfärdigheter under samtalet. Men nej, det går inte. Han pratar, men Oscar förstår inte. Det verkar inte finnas en tydlig röd tråd. Efter ca 10 minuter, skriker patienten plötsligt:
~inhamtatInformation = inhamtatInformation+3
    "De är döda!"
-> h1_samtal_devam_2




== h1_samtal_devam_2

{agitationsprovocerare >=3:
    - -> agiterad_action
}
<br>Han säger fort att det är grannarna som är döda. Han börjar hyperventilera och uppger att de mördades av CIA.   
~inhamtatInformation = inhamtatInformation+5
* <b>"Mördades?"</b> [] frågar Oscar med orolig blick. 
~speglingAntal += 1
# images/question.png
~fragar_mordades = true
~professionalism_score = professionalism_score+2
~samtalsfardigheter = samtalsfardigheter+5
Patienten beskriver snabbt att det egentligen är FBI som gjorde detta. De har kontakt med SÄPO. Då de sedan länge använt en avlyssningsapparat och kamerautrustning hemma hos honom, hörde de honom säga vissa saker och tolkade dessa fel. Det var därför de mördade grannarna. Det är alltså inte hans fel.

Alltså CIA, FBI, eller SÄPO?.. 
~inhamtatInformation = inhamtatInformation+10
Oscar väljer nu att fråga:
-> h1_samtal_devam_3

* <b> "Ta det lugnt nu. Jag vet att du talar sanning och tror på vad du säger." [] </b>säger Oscar. 
~jag_tror_pa_dig_johan = true
~professionalism_score = professionalism_score-2
~samtalsfardigheter = samtalsfardigheter-3
    "Hur vet du det?" frågar han. Hmmm... Svår fråga för Oscar. 
    "Det är nog inte så svårt att ordna en kamerautrustning här. Jag <b>vet</b> vad ni håller på med. Men det kommer inte hända. DU KAN INTE LURA MIG! Jag visste att du är en av dem. Jag går härifrån." 
~inhamtatInformation = inhamtatInformation+5
Oj, det gick fort! Så patienten vill gå därifrån. Går bestämt mot dörren.
-> vill_ga_darifran

VAR h1_forsoker_validera = false 
VAR h1_konfronterar_direkt = false 






== h1_samtal_devam_3

{agitationsprovocerare >=3:
    - -> agiterad_action
}
* <b> "Kan du berätta lite mer om dina grannar?" [] </b>frågar Oscar. 
Han beskriver att han ständigt hör röster som tvingar honom att säga att grannarna förtjänar att bli dödade. 
~fokuserar_pa_utomjord = true
~inhamtatInformation = inhamtatInformation+10
~professionalism_score = professionalism_score+2
~samtalsfardigheter = samtalsfardigheter+5
-> h1_samtal_devam_3

* <b> "Du sa att det är FBI som dödade dina grannar. Är du 100% säker på det? Vad har du för bevis? " [] </b>frågar Oscar. 
~fokuserar_pa_evidens = true
Han säger att han är 100% säker på det. Men det gjorts i samarbete mellan med CIA och FBI, samt SÄPO. 
~inhamtatInformation = inhamtatInformation+5
~professionalism_score = professionalism_score-5
~samtalsfardigheter = samtalsfardigheter-5
-> h1_samtal_devam_3

* <b> "Tror du verkligen på det här?" []</b> frågar Oscar. 
~h1_konfronterar_direkt = true 
~agitationsprovocerare = agitationsprovocerare + 1 
~professionalism_score = professionalism_score-5
~samtalsfardigheter = samtalsfardigheter-5
Inget svar. 
-> roster 

~fokuserar_pa_apparat = true
* <b> "Du sa att det finns en avlyssningsapparat och kamerautrustning hemma hos dig. Kan du berätta mer om det?" [] </b>säger Oscar för att förtydliga detta lite mer. 
# images/camera.png
Han säger att det är en apparat som CIA installerade för några månader sedan. De kan lyssna av honom när de vill. 
~inhamtatInformation = inhamtatInformation+10
~samtalsfardigheter = samtalsfardigheter+5
-> h1_samtal_devam_3

* <b> "Det måste vara jobbigt att ha sådana upplevelser." [] </b>säger Oscar.
~h1_forsoker_validera = true 
~professionalism_score = professionalism_score+2
~samtalsfardigheter = samtalsfardigheter+3
Oscar tror inte patienten hörde vad han sa. Han tycker det ändå var ett bra försök att förmedla medkänsla!
-> roster 

* <b> "Du talar för mig att dina grannar är döda. Men vi vet att de inte är döda. Vad säger du om det?" [] </b>säger Oscar och vänder sig mot poliserna för bekräftelse. 
~h1_konfronterar_direkt = true 
~professionalism_score = professionalism_score-3
~samtalsfardigheter = samtalsfardigheter-5
Polismän bekräftar att de inte är döda. 

Han verkar vara mer orolig efter att han hör detta. "Hur vet ni det?" frågar han. "Det är nog inte så svårt att fixa en kamerautrustning här. Jag <b>vet</b> vad ni håller på med. Men det kommer inte hända. Jag går härifrån." 
-> vill_ga_darifran




== roster
{agitationsprovocerare >=3:
    - -> agiterad_action
}
Oscar bestämmer sig att fråga patienten mer om rösterna. 

* <b> "Kan du berätta lite mer om rösterna?" [] </b>frågar Oscar.
Patienten säger att han inte längre vill prata med honom.
~professionalism_score = professionalism_score+5
~samtalsfardigheter = samtalsfardigheter+5
~time_score += -5
Oscar försöker prata lite mer med honom, men nej, det går inte. 
~inhamtatInformation = inhamtatInformation+5
-> roster_2

== roster_2
{laser_journal:
    - -> vill_ga_darifran
    - else: -> roster_3
    }
    == roster_3
Oscar väljer nu... # fa:fas fa-arrow-circle-down
->kolla_journal



VAR laser_journal = false 

== kolla_journal
*[... att läsa patientens journal.]
-> medicinsk_anamnes

== medicinsk_anamnes
~laser_journal = true 
~inhamtatInformation = inhamtatInformation+7
<b> Oscar öppnar patientens journal. </b>

Patienten har en aktuell schizofrenidiagnos.... OK.... Han blev ordinerad <i>Risperdal 4 mg</i> men uteblev från de två senaste återbesöken.... Hmmm..... Patientens två bröder har schizofreni och behandlas med Klozapin..... Inget drogmissbruk finns uppgivet. 
-> vill_ga_darifran






// VILL GÅ DÄRIFRÅN

=== vill_ga_darifran ===
# images/lock.png

Så han vill absolut inte vara här. Vill att de öppnar dörren. Säger att de inte får behålla honom här för det är olagligt. 

* Han får gå. Oscar kan planera ett mottagningsbesök för snar uppföljning. <b><br> "Trevligt att träffa dig, Johan.  " [] </b>säger Oscar, de öppnar dörren.
~stor_kunskapsbrist = true
~professionalism_score = professionalism_score - 5
~time_score += -5
~kunskap_om_lpt = kunskap_om_lpt-30
-> game_over


* Han får inte gå. Oscar bedömer att kriterier för LPT-vård nog är uppfyllda. . <br><b> "Johan, du måste stanna kvar." [] </b>säger Oscar. OK, men hur??
~far_inte_ga = true
~samtalsfardigheter = samtalsfardigheter+4 
~kunskap_om_lpt = kunskap_om_lpt+10
-> behaller_patienten


VAR h1_fattar_kvarhallning_fore_vardintyg = false 
VAR h1_fattar_omhandertagandebeslut = false 


== behaller_patienten
# images/lagrum.png
Oscar bedömer att patienten måste stanna här på sjukhuset. Men på vilket sätt? Enligt vilket lagrum? Oscar känner sig inte helt kompetent i detta. <br>
<i>Det vore klokt att uppdatera sig lite om LPT innan Oscar väljer någon handling. 
<br> </i>

-> behaller_patienten_val

==behaller_patienten_val

Oscar väljer nu... # fa:fas fa-arrow-circle-down

* [... att skriva vårdintyg.]
~skriver_vardintyg = true
~kunskap_om_lpt = kunskap_om_lpt-20
~time_score += -10
-> game_over

* [... att kontakta bakjour.]
Oscar väljer att kontakta bakjour.
~kontaktar_bakjour_fore_vardintyg = true
~kunskap_om_lpt = kunskap_om_lpt-10
~time_score += -5
-> ring_bakjour_fore_vardintyg

* {h1_fattar_omhandertagandebeslut} [... att kontakta ST-läkare som är LPT-jour]
Oscar väljer att kontakta ST-läkare som är LPT-jour.
~kontaktar_ST_lakare = true
~time_score += 5
{
- not kontaktar_bakjour_fore_vardintyg && not skriver_vardintyg && not h1_fattar_kvarhallning_fore_vardintyg:
    ~kunskap_om_lpt = kunskap_om_lpt+20
- else:
    ~kunskap_om_lpt = kunskap_om_lpt+5
}
-> ring_leg_lakare

* [... att fatta kvarhållningsbeslut]
~h1_fattar_kvarhallning_fore_vardintyg = true  
Oscar får tyvärr inte fatta kvarhållningsbeslut innan det finns ett vårdintyg. 
~kunskap_om_lpt = kunskap_om_lpt-5
-> behaller_patienten

* [... att fatta omhändertagandebeslut]
~h1_fattar_omhandertagandebeslut = true 
~time_score += 3
{
- not kontaktar_bakjour_fore_vardintyg && not skriver_vardintyg && not h1_fattar_kvarhallning_fore_vardintyg:
    ~kunskap_om_lpt = kunskap_om_lpt+20
- else:
    ~kunskap_om_lpt = kunskap_om_lpt+5
}
Att fatta omhändertagandebeslut! <b>Det är nog en bra idé! </b><br>
Oscar meddelar patienten, polisen och övriga personal att han fattar ett omhändertagandebeslut. 
-> behaller_patienten_val


== otillracklig_anamnes_till_kollega
Oscar inser plötsligt att han har mycket liten information om patientens anamnes. # fa:fas fa-frown fa-2x
<br>
-> game_over



// BAKJOUR START
== ring_bakjour_fore_vardintyg
"Hej, dr Massoud här, psykiatri bakjour?"

* "Hej, det är AT-läkare Oscar Johansson som ringer. " [] 
"Hej, Oscar! Hur kan jag hjälpa dig?"
-> rapport_till_bakjour 

== rapport_till_bakjour
{

      - inhamtatInformation<17:
    ~patAnamnes = patAnamnes.low
    
    - inhamtatInformation>=17 && inhamtatInformation<=26:
    ~patAnamnes = patAnamnes.nope  
    
      - inhamtatInformation>26 && inhamtatInformation<=39:
    ~patAnamnes = patAnamnes.average 
    
       - inhamtatInformation>39 && inhamtatInformation<=52:
    ~patAnamnes = patAnamnes.high 
    
       - inhamtatInformation>52 && inhamtatInformation<=65:
    ~patAnamnes = patAnamnes.perfekt 
}


{
    - patAnamnes == low: 
    -> otillracklig_anamnes_till_kollega
    
    - else:
    

*Jo, det är så här. Här finns en schizofren patient som har kommit med polisen. Jag kan inte göra en bedömning eftersom han vill inte stanna kvar.
~jag_kan_inte_bedoma = true
~professionalism_score = professionalism_score-4
-> kan_inte_bedoma_bakjour

* Jo, det är så här. Här finns en man med schizofreni, kommit nu med polisen. Har inte tagit sina mediciner på ett tag. Han är nu psykotisk. Vill inte vara här. Jag behöver dig här för en bedömning.
-> kan_inte_bedoma_bakjour

* Jo, det är så här. Jag har en manlig patient som har kommit med polisen. Har haft kontakt med psykiatri, diagnosen har varit schizofreni. Nu har han inte tagit sina mediciner, pratar om konstiga saker och vill inte vara här. Jag tycker att han måste stanna här. 
~professionalism_score = professionalism_score+3
-> kan_inte_bedoma_bakjour

* Jo, det är så här. Jag har träffat en manlig patient som ger mycket dålig kontakt. Har schizofreni men har inte tagit sina mediciner på ett tag. Han är psykotisk, vill inte vara här. 
~professionalism_score = professionalism_score+2
-> kan_inte_bedoma_bakjour


}
    

== kan_inte_bedoma_bakjour
"Jag antar att han inte är LPT-mässig?" säger bakjour. 
<br>
* "Nej, det tycker jag inte" [] svarar Oscar.
~kunskap_om_lpt = kunskap_om_lpt-20
~professionalism_score = professionalism_score-10
-> game_over
* "Vet faktiskt inte." [] säger Oscar.
Hon ber Oscar att kontakta ST-läkare i psykiatri, men han förstår inte varför... Hmm... 
~kunskap_om_lpt = kunskap_om_lpt-10
-> self_reflektion
* "Han uppfyller nog LPT-kriterier." [] säger Oscar. 
~kunskap_om_lpt = kunskap_om_lpt+10
dr Massoud ber Oscar att kontakta ST-läkare i psykiatri, för att det är det första man ska göra. OK. -> self_reflektion 




== self_reflektion
<i>Att kontakta bakjour innan han kontaktar ST-läkare var uppenbarligen en dålig idé. 
Oscar vet i alla fall vad han ska göra nu. 

Det som behövs är att kontakta en legitimerad läkare. 
-> ringer_lpt_jour_efter 

== ringer_lpt_jour_efter
Han väljer att kontakta ST-läkare.
-> ring_leg_lakare



== ring_leg_lakare
{

      - inhamtatInformation<17:
    ~patAnamnes = patAnamnes.low
    
    - inhamtatInformation>=17 && inhamtatInformation<=26:
    ~patAnamnes = patAnamnes.nope  
    
      - inhamtatInformation>26 && inhamtatInformation<=39:
    ~patAnamnes = patAnamnes.average 
    
       - inhamtatInformation>39 && inhamtatInformation<=52:
    ~patAnamnes = patAnamnes.high 
    
       - inhamtatInformation>52:
    ~patAnamnes = patAnamnes.perfekt 
    
    - else: 
    ~patAnamnes = patAnamnes.low
}

{
    - patAnamnes == low: 
    -> otillracklig_anamnes_till_kollega
    
    - else:
Vi får se... ST-läkare dr Karin Winblad. 
Hon svarar: 
"Hej, dr Karin Winblad här, psykiatri ST-läkare?"
* <b> "Hej, det är AT-läkare Oscar Johansson, primärjour som ringer. " [] <br>
"Hej, Oscar! Hur kan jag hjälpa dig?"<br>
Oscar rapporterar patienten till ST-läkare enligt SBAR. Föredömligt. 
-> rapport_till_LPT_jour
    
}


== rapport_till_LPT_jour

* <b>"Det gäller en patient som har kommit hit med polisen. Har schizofrenidiagnos, planerade uppföljningar men har uteblivit från de senaste besöken. Han är nu psykotisk, har vanföreställningar, rösthallunicationer. Han vill dessutom gå härifrån. Så jag tycker att det behövs en vårdintygsbedömning. "</b><br>
<b>Karin:</b> "Hmm, OK. Jag kommer med en gång!"
<b>Oscar:</b>"OK! Tack!"
~time_score += 5
-> slut_pa_oscar 



== slut_pa_oscar
... 
Det tar ca en kvart innan Karin dyker upp. Oscar känner sig lugnare. Nu är det slut med hans del för denna patient. 

* dr Winblad är redo för sin bedömning. Vi går vidare! <br>
-> LPT_jour_kommer

VAR game_over_oldu = false 

=== game_over
~game_over_oldu = true 
# images/gameover.png
<br>
<br>
<h4><b><center><i>Vad kul att du provade spelet! <br>

<center>Lycka till med ditt nya försök! <br><br>

-> fb_oscar
}



