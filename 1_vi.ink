

== 1_vi_1
# images/1_vi_1.png 
Dr Winblad måste välja att kryssa för ett av följande alternativ:

* [4§]
Hon väljer 4§. 
~valjer_paragraf_4 = true
-> 1_vi_2

* [11§]
Hon väljer 11§.
~valjer_paragraf_11 = true
-> 1_vi_2

* [5§]
Hon väljer 5§.
~valjer_paragraf_5 = true
-> 1_vi_2


== 1_vi_2
# images/1_vi_2.png 

dr Winblad måste nu skriva om hur identiteten styrkts. Hon vet dock inte om/hur man har gjort detta. Hon väljer för detta fält att...

* [... fylla i fältet med "ID-kort" för det är mest sannolikt.]
dr Winblad väljer att skriva "ID-kort" för detta fält. Hon vet faktiskt inte om detta, men gissar.  
~valjer_id_kort = true

-> 1_vi_3
* [... inte fylla i detta fält för det är lite bråttom.]
dr Winblad väljer att inte skriva något i detta fält. 
~valjer_att_inte_skriva_nagot = true

-> 1_vi_3
* [... fråga expeditionen och/eller polisen om de har kontrollerat ID-kortet.]
dr Winblad väljer att undersöka om man tidigare har kontrollerat ID-kortet. Jo, det har de gjort. Patienten saknar ID-kort, har tappat bort detta, men han har körkort. Skriver därför "Körkort" för detta fält.
~valjer_att_undersoka_vidare = true 
-> 1_vi_3




== 1_vi_3
# images/1_vi_3.png 

Nästa fält i vårdintyget handlar om ANAMNES/BAKGRUND. 

dr Winblad väljer att skriva...

* [Man med psykotiska symtom, vanföreställningar och rösthallucinationer.]
dr Winblad väljer att skriva "Man med psykotiska symtom, vanföreställningar och rösthallucinationer." 
~bakgrund_skriver_aktuellt =true
-> 1_vi_4

* [40-årig man som har oundgängligt behov av att stanna på sjukhuset på grund av schizofreni.]
dr Winblad väljer att skriva "40-årig man som har oundgängligt behov av att stanna på sjukhuset på grund av schizofreni."
~bakgrund_skriver_behov_att_stanna = true 
-> 1_vi_4

* [Man med känd schizofreni, bor med föräldrarna. Har tidigare varit inlagd enligt LPT flera gånger. ]
dr Winblad väljer att skriva "Man med känd schizofreni, bor med föräldrarna. Har tidigare varit inlagd enligt LPT flera gånger."
~bakgrund_skriver_korrekt = true 
-> 1_vi_4

* [Man med schizofreni kommer idag med polis. Vill lämna sjukhuset omedelbart. ]
dr Winblad väljer att skriva "Man med schizofreni kommer idag med polis. Vill lämna sjukhuset omedelbart."
~bakgrund_skriver_kommer_med_polis = true 
-> 1_vi_4




== 1_vi_4
# images/1_vi_4.png 

Nästa fält i vårdintyget handlar om ANAMNES/AKTUELLT.
dr Winblad vill gärna vara kort och koncist. 

Hon väljer att skriva...

* [Kommer idag med polis. Har aktiva hallucinationer och vanföreställningar. Beskriver dålig läkemedelskomplians. Kan inte medverka till ett samtal. Behöver stanna kvar. ]
~aktuellt_bedomer = true 
-> 1_vi_5 

* [Kommer idag med polis. Hittades i stadsskogen. Varit försvunnen sedan några dagar tillbaka. Påstår att ha en avslyssningsapparat hemma hos honom och hans grannar mördades av utomjordingar. Enligt journaluppgifter har schizofreni och det framkommer att han har slutat med sina mediciner. ]
~aktuellt_korrekt = true  
-> 1_vi_5

* [Kommer idag med polis. Hittades i stadsskogen. Schizofren. Lider av allvarlig psykisk störning. Vill lämna sjukhuset omedelbart men behöver stanna kvar för observation. ]
~aktuellt_kort_daligt = true 
-> 1_vi_5

* [Kommer idag med polis. Bedöms ha bisarra vanföreställnignar och rösthallucinationer vilket innebär störd realitetsuppfattning. Uppger att grannarna mördades av utomjordingar. ]
~aktuellt_bedomer_men_ok = true  
-> 1_vi_5




VAR status_svar_1 = false
VAR status_svar_2 = false
VAR status_svar_3 = false
VAR status_svar_4 = false

== 1_vi_5
dr Winblad fyller sedan i "STATUS". 

För somatisk status, väljer hon att skriva att det inte är möjligt att i nuläget göra en somatisk undersökning, men att patienten verkar neurologiskt, respiratoriskt, cirkulatoriskt stabil. 

För psykisk status, tänker hon olika rubriker i psykisk status. Försöker göra sitt bästa under tidspress. Hon skriver...

* [Manlig patient, har dålig självhygien. Affekterna är stabila. Grundstämningen är neutral. Adekvat formell och emotionell kontakt. Ter sig psykotisk. Dålig komplians.] 
~status_svar_1 = true 
-> 1_vi_6


* [Ung man. Adekvat formell och emotionell kontakt. Ordinärt tankeförlopp. Hallucinationer och vanföreställningar finns. Inget suicidalt. Bristfällig sjukdomsinsikt. Vill lämna sjukhuset. ]
~status_svar_2 = true 
-> 1_vi_6


* [Manlig patient, har dålig hygien, luktar ofräsht. Ger avvikande kontakt. Har vanföreställningar i form av att hans grannar mördades av utomjordingar och det finns en avlyssningsapparat hos honom. Har även rösthallucinationer. Obefintlig sjukdomsinsikt. ]
~status_svar_3 = true 
-> 1_vi_6

* [Patient med bristande själv-hygien. Pågående hallucinationer och bisarra vanföreställningar. Klar, vaken och orienterad. Acceptabel kontakt. Dålig insikt. Inget suicidalt.  ]
~status_svar_4 = true 
-> 1_vi_6



VAR sammanfattning_1_svar_1 = false 
VAR sammanfattning_1_svar_2 = false 
VAR sammanfattning_1_svar_3 = false 
VAR sammanfattning_1_svar_4 = false 

==1_vi_6
dr Winblad fyller sedan i sammanfattning i vårdintyget.

1) Den undersökte lider av allvarlig psykisk störning p.g.a. :
* [Hon väljer att skriva en diagnos, typ "Akut psykos"]
~sammanfattning_1_svar_1 = true
-> DONE

* [Hon väljer att beskriva patienten, typ "Manlig patient med vanföreställningar, hallucinationer... "]
~sammanfattning_1_svar_2 = true
-> DONE

* [Hon väljer att skriva "Kriterion är uppfyllt"]
~sammanfattning_1_svar_3 = true
-> DONE



==1_vi_7
dr Winblad fyller sedan nästa fältet i intyget.

Den undersökte är i oundgängligt behov av kvalificerad psykiatrisk dygnetruntvård och går inte att behandla i öppenvård (t.ex. i hemmet, i särskilda boendeformer, på behandlingshem eller på en sjukvårdinrättning för somatisk vård) p.g.a.:

* [dr Winblad beskriver varför det finns ett behov av att patienten måste stanna och behöver behandling på sjukhuset. Hon nämner att patienten inte har sjukdomsinsikt, så det är omöjligt att han ska följa läkarordinationen. Hon nämner även att patienten kan agera enligt sina vanföreställningar, vilket kan vara farligt. ]
-> 1_vi_8 





VAR sammanfattning_installning_1 = false 
VAR sammanfattning_installning_2 = false 
VAR sammanfattning_installning_3 = false 

== 1_vi_8
Hon fyller sedan i det tredje kapitlet i sammanfattningen, nämligen patientens inställning till erbjuden vård. Hon väljer att skriva...

* [Patienten är inte hjälpsökande. ]
~sammanfattning_installning_1 = true 
-> 1_vi_9

* [Patienten vägrar erbjuden vård.]
~sammanfattning_installning_2 = true 
-> 1_vi_9

* [Patienten är ej kapabel att fatta ett grundat beslut om erbjuden vård.]
~sammanfattning_installning_3 = true 
-> 1_vi_9





== 1_vi_9
dr Winblad känner sig lite osäker om vad hon ska göra med nästa fältet i intyget. 

Hon känner behov av att kolla om hon ska fylla i detta fälte eller inte. 

* [Hon läser lite om LPT, kollar sina anteckningar och kommer tillbaka. Det kan man verkligen göra om man känner sig osäker!]
-> 1_vi_10



== 1_vi_10
Hon väljer att inte fylla i detta fälte för denna patient. Nu vet hon varför. 

Nu är det sista delen kvar, Cosmic har redan fyllt i många fält som address, tjänsteställe, postnummer osv.

Hon vet att det är mycket viktigt med att kryssa för "SVENSK LEGITIMATION" varför hon gör detta. 

Hon är nästan färdig!

* [Hon "SIGNERAR" nu intyget i Cosmic, vilket betyder att det är slut med processen!]
-> 1_vi_11

* [Hon "SIGNERAR" intyget och skriver ut för att skriva under för hand.]
-> 1_vi_12


== 1_vi_11
-> game_over

== 1_vi_12
Hon signerar intyget, skriver ut och skriver under. Det var verkligen viktigt att hon inte glömde att skriver under intyget! Själva intyget måste ju vara till hands! 
-> inlaggning



VAR st_fattar_intagning = false 

VAR st_fattar_omhantertagande = false 

== inlaggning
dr Winblad dikterar en inskrivningsanteckning och rapporterar patienten till den akuta inläggningsenheten i psykiatri. Patienten vill fortfarande gå. dr Winblad beslutar om att patienten ska stanna och fattar ett beslut om...

* [Kvarhållning enligt 6a§ LPT]
Hon fattar kvarhållningsbeslut. 
-> DONE

* [Intagningsbeslut enligt 6b§ LPT]
~st_fattar_intagning = true 
dr Winblad får inte göra det. Hon är ju inte "chefsöverläkare" eller en specialistläkare som har fått en delegation om behörigheterna. 

Cosmic påminner dr Winblad om att man inte kan fatta ett intagningsbeslut innan man fattar kvarhållningsbeslut. Så hon måste fatta ett beslut om... 
-> inlaggning

* [Omhändertagande...]
~st_fattar_omhantertagande = true 
dr Winblad verkar inte alls koncentrerad. Tur att hon kunde klara av att skriva ett ordentligt vårdintyg! 

Det är verkligen inte omhändertagandebeslut som hon ska fatta, utan...
-> inlaggning
